$(document).ready(function(){
  console.log("Document loaded, running js...");
  isMobile = Modernizr.mq('(max-width: 40em)');
  if (isMobile){
    $('#innerwrap').append($("#pagecontent").detach().html());
  } else {
    console.log("Is not a mobile device, no off-canvas menu needed!");
    //$("body").append($("#pagecontent").detach().html());
  }
  
  console.log("Starting Foundation JS...");
  $(document).foundation();
  
  console.log("That's us for now... Moving on.");
  
});
